# Tool Package Includes:

_180067_ProceduralTileGenSystem =>
> Prebabs =>

---CheckPointManager---
CheckPoint
> Scripts =>

CheckPointSystem
CheckPointTrigger
CourseGenManager
Tile
	
## Process 
The system was designed with the idea to be able to generate tiles that can either be quick rooms to go through, or entire level points for the player to explore. And the Generator is meant to not only generate them but link them together. The tiles are your own design and can be as simple or complex as you need them to be, and they'll generate endlessly. The system itself can be modified to suit the needs of your game.

## Purpose
The Procedural Tile Generator system is meant to assemble a linear course consisting of your own tiles that are generated in a random sequence. 
Your tiles can have their own scripts attached along with the Tile script to add your own level of complexity.

## Tile Requirements
- Tiles will need the Tile script attached to them.
- Tiles will need to be a prefab
- They will need a trigger collider to be around them to detect when the player has entered, to generate the next Tile.
- Tiles will need at least one empty object parented to them to act as a spawn link for the next tile.
- The tile's parts/components must be parented to a single object that is a prefab, in order to be spawn ready for the tile before it. 

## How to Use Procedural Tile Generator

1. Simply drag the **180067_ProceduralTileGenSystem** package into the project's assets folder.
> **Note:** Make sure all the checkboxes are ticked in the import options.

2.  Find the package's Prefabs folder, and drag the CheckpointManager Prefab into the scene and reset its transform position to 0, 0, 0.  Then drag the Checkpoint prefab where you want your player to restart. And on the CheckpointManager, assign the player and add the checkpoint to the list of Checkpoints. Also, set a negative minimum value of how far the player has to fall to be reset. 
> **Note:** The player requires a Rigidbody component to fall and be reset 

3. Set up the starting area where you want your tiles to start spawning by placing the CourseGenManager script onto an area where the player will walk e.g. underneath an arch etc. Place the script on the arch and a trigger collider on it that the player can't miss. Remember to add Spawn links to the Gen Manager to start the course. And add at most two tile prefabs to start the course spawning with. The tiles will communicate with the Gen Manager to manage them, through the Tile script. Set the Maximum Tile Amount to optimize the game by only allowing a preset amount of tiles to exist at once.
> **Note:** Place the first Checkpoint at the start of the course for testing purposes.

4.  Build your own tiles according to the Tile Requirements above. The tiles can look any way you want and can be as big as you want. 

5. (Recommended) To make your tiles spawn ready, place all of the tile's parts/components into an empty object with the name of the tile. Then line up the location of the empty object with the spawn link of possible previous tiles to ensure the wanted results. Do the same for the Gen Manager's links if needed.

6. When you have more than one tile ready but your Links don't line up with the prefab's parent, make use of multiple spawn links for each tile where needed and line up their possible links for each one. Then use the switch case in the Tile script to ensure the proper tile spawns at its designated position.

> **Note:**  The system's linear structures are meant to be modified to meet the requirements of your game. See attached ExampleProject and fellow its ReadMe to install. (You can use a separate project for the Example project.)