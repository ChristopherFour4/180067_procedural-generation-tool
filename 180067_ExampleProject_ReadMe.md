# Example Project Package Includes:

_180067_ProceduralSystem_ExampleProject =>
	Example Project =>
 - Animations 
 - ExampleGame_Scripts
 - Fonts
 - Images
 - JMO Assets (Visual Effects from asset store. Original Creator: **Jean Moreno** (JMO))
 Link: https://assetstore.unity.com/packages/vfx/particles/war-fx-5669
 - Materials 
 - Models
 - Physics
 - Pistol (Pistol from asset store. Original Creator: **Stanislav Marakulin**)
 Link: https://assetstore.unity.com/packages/3d/props/guns/handgun-26689
 - Prefabs
 - Scenes
 - Sound
 - SpaceSkies (Skyboxes from asset store. Original Creator: **PULSAR BYTES**)
 Link: https://assetstore.unity.com/packages/2d/textures-materials/sky/starfield-skybox-92717 

 Procedural_System_Scripts

## Prerequisites for Example Project ONLY
- Upgrade your project to URP scene if you're using a 3D scene.
- Install probuilder from package manager.


## How to Use Example Project

1. Simply drag the **180067_ProceduralSystem_ExampleProject** package into the project's assets folder.
> Make sure all the checkboxes are ticked in the import options.

2. Install **ProBuilder** from the **Package Manager**. 

3.  Open the **Gunswinger** scene in the scenes folder that came with the package.

4. Once in the example game, press [Escape] to bring up the pause menu for the game's controls.

> **Note:** The Procedural Tile Generator is on the upper level of the game. On start, look up and make your way to the second level using the cubes and grapling-gun 